defmodule KDT do
  @moduledoc """
  K-D Tree implementation using the standard split method
  (infers the dimension based on the size of the first point)

  point is a tuple representing a point
  data is some associated value to that point (defaults to nil)

  KDT.new([%KDT{point: {0.5, 0.3}, data: "blue sky"}, %KDT{point: {0.6, 0.8}, data: "red roses"}])
  """

  @enforce_keys [:point]
  defstruct point: nil, data: nil, left: nil, right: nil
  @type t :: %__MODULE__{point: tuple(), data: any(), left: nil|KDT.t()|Task.t(), right: nil|KDT.t()|Task.t()}

  def new(%__MODULE__{} = kd_tree), do: kd_tree

  def new(list) do
    target = ceil(:math.log2(System.schedulers_online())) + 1

    list
    |> partition(target)
    |> await_partitions(100_000)
  end


  defp partition(list, depth \\ 0, target)
  defp partition([], _depth, _target), do: nil
  defp partition([node], _depth, _target), do: node

  defp partition(list, depth, target) when depth === target do
    Task.async(fn -> partition(list, depth, nil) end)
  end

  defp partition(list, depth, target) do
    axis = rem(depth, dimensions(list))

    m =
      list
      |> Enum.map(fn %KDT{point: p} -> elem(p, axis) end)
      |> Median.select

    {left, rest} =
      Enum.split_with(list, fn %KDT{point: p} -> elem(p, axis) < m end)

    {right, root} =
      Enum.split_with(rest, fn %KDT{point: p} -> elem(p, axis) !== m end)
      |> case do {a, [b | c]} -> {a++c, b} end

    root
    |> Map.put(:left, partition(left, depth+1, target))
    |> Map.put(:right, partition(right, depth+1, target))
  end


  defp await_partitions(nil, _timeout), do: nil
  defp await_partitions(%Task{} = root, timeout), do: Task.await(root, timeout)

  defp await_partitions(%KDT{} = root, timeout) do
    root
    |> Map.put(:left, await_partitions(root.left, timeout))
    |> Map.put(:right, await_partitions(root.right, timeout))
  end


  def insert(tree, node, depth \\ 0)
  def insert(nil, node, _depth), do: node

  def insert(tree, node, depth) do
    axis = rem(depth, dimensions(tree))

    child =
      (if elem(node.point, axis) < elem(tree.point, axis),
        do: :left,
        else: :right)

    %{tree | child => insert(Map.get(tree, child), node, depth+1)}
  end


  @doc """
  Nearest neighbor search.
  """
  def nearest(%KDT{} = root, point) do
    best_d = distance(root.point, point)
    best_n = root

    [best_d | best_n] = nearest(root, point, best_d, best_n, 0)
    best_n = best_n |> Map.put(:left, nil) |> Map.put(:right, nil)

    {best_d, best_n}
  end

  defp nearest(nil, _point, best_d, best_n, _depth), do: [best_d | best_n]

  defp nearest(root, point, best_d, best_n, depth) do
    axis = rem(depth, dimensions(root))

    a = elem(point, axis)
    b = elem(root.point, axis)
    axis_d = abs(a - b) # distance from point to current axis

    root_d = distance(root.point, point)
    [best_d | best_n] =
      (if root_d < best_d,
        do: [root_d | root],
        else: [best_d | best_n])

    if best_d < axis_d do # i.e. doesn't cut the hyperplane (check only one side)
      if a < b do
        nearest(root.left, point, best_d, best_n, depth+1)
      else
        nearest(root.right, point, best_d, best_n, depth+1)
      end
    else # i.e. does cut the hyperplane (so maybe check both sides...)
      # We chose which child we search first based on which side of the split hypersphere has more volume
      {c1, c2} = (if a < b, do: {:left, :right}, else: {:right, :left})
      [best_d | best_n] = nearest(Map.get(root, c1), point, best_d, best_n, depth+1)

      if best_d < axis_d do # i.e. does the new best cuts the hyperplane
        [best_d | best_n]
      else
        nearest(Map.get(root, c2), point, best_d, best_n, depth+1)
      end
    end
  end

  @doc """
  Searches points that are inside the given hypersphere.
  Returns a list of %KDT{}'s.
  """
  def radius_search(%KDT{} = node, point, radius)
  when is_tuple(point) and is_float(radius) do
    radius_search(node, point, radius, 0)
  end

  defp radius_search(nil, _point, _radius, _depth), do: []

  defp radius_search(node, point, radius, depth) do
    axis = rem(depth, dimensions(node))
    a = elem(point, axis)
    b = elem(node.point, axis)

    l =
      if abs(a - b) <= radius do
        radius_search(node.left, point, radius, depth+1)
        ++ radius_search(node.right, point, radius, depth+1)
      else
        if a < b do
          radius_search(node.left, point, radius, depth+1)
        else
          radius_search(node.right, point, radius, depth+1)
        end
      end

    #? si la distancia entre el eje de la seccion y el punto es mayor al radio, entonces no hace falta esto:
    (if distance(node.point, point) < radius,
      do: [%KDT{point: node.point, data: node.data} | l],
      else: l)
  end

  @doc """
  Searches points that are inside the given k-orthotope.
  Precondition: {{a, b}, ...} where a < b
  Returns a list of %KDT{}'s.
  """
  def range_search(node, box, depth \\ 0)
  def range_search(nil, _box, _depth), do: []

  def range_search(node, box, depth) do
    axis = rem(depth, dimensions(node))
    {a, b} = elem(box, axis)
    p = elem(node.point, axis)

    cond do
      p < a ->
        range_search(node.right, box, depth+1)
      p > b ->
        range_search(node.left, box, depth+1)
      true ->
        l = range_search(node.left, box, depth+1)
        r = range_search(node.right, box, depth+1)
        [%KDT{point: node.point, data: node.data} | l ++ r]
    end
  end


  # calculates euclidean distance between two given points
  defp distance(p1, p2) do
    sum =
      1 .. tuple_size(p1)
      |> Enum.reduce(0, fn x, acc ->
        :math.pow((elem(p1, x-1) - elem(p2, x-1)), 2) + acc
      end)

    :math.sqrt(sum)
  end

  defp dimensions(%KDT{point: p}), do: tuple_size(p)
  defp dimensions([%KDT{point: p} | _]), do: tuple_size(p)

  def height(nil), do: 0
  def height(n), do: max(height(n.left), height(n.right)) + 1

  def size(nil), do: 0
  def size(%KDT{} = kdt), do: size(kdt.left) + size(kdt.right) + 1

  def member?(nil, _val), do: false
  def member?(%KDT{point: p}, val) when p === val, do: true
  def member?(%KDT{} = kdt, val), do: member?(kdt.left, val) or member?(kdt.right, val)

  def to_list(nil), do: []
  def to_list(%KDT{} = kdt), do: List.flatten([to_list(kdt.left), kdt, to_list(kdt.right)])


  defimpl Enumerable do
    def count(kdt) do
      {:ok, KDT.size(kdt)}
    end

    def member?(kdt, val) do
      {:ok, KDT.member?(kdt, val)}
    end

    def slice(kdt) do
      size = KDT.size(kdt)
      {:ok, size, &Enumerable.List.slice(KDT.to_list(kdt), &1, &2, size)}
    end

    def reduce(kdt, acc, fun) do
      Enumerable.List.reduce(KDT.to_list(kdt), acc, fun)
    end
  end

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(node, opts) do
      opts = %Inspect.Opts{opts | width: 0}

      n = [point: node.point]
      n = (if node.data !== nil, do: n ++ [data: node.data], else: n)
      n = (if node.left !== nil, do: n ++ [left: node.left], else: n)
      n = (if node.right !== nil, do: n ++ [right: node.right], else: n)

      open = color("#" <> inspect(@for) <> "<", :map, opts)
      sep = color(",", :map, opts)
      close = color(">", :map, opts)

      container_doc(open, n, close, opts, traverse_fun(n, opts), separator: sep, break: :strict)
    end

    defp traverse_fun(list, opts) do
      if Inspect.List.keyword?(list) do
        &Inspect.List.keyword/2 # This is always returned
      else
        sep = color(" => ", :map, opts)
        &to_map(&1, &2, sep)
      end
    end

    defp to_map({key, value}, opts, sep) do
      concat(concat(to_doc(key, opts), sep), to_doc(value, opts))
    end
  end
end
