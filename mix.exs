defmodule KDT.MixProject do
  use Mix.Project

  def project do
    [
      app: :kdt,
      version: "0.2.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/trobador/kdt",
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:median, "~> 0.1.1"}
    ]
  end

  defp description() do
    "KDT with nearest neighbor, radius, and range search."
  end

  defp package() do
    [
      files: ["lib", "mix.exs", "LICENSE", "README.md"],
      licenses: ["GNU GPLv3"],
      links: %{"GitLab" => "https://gitlab.com/trobador/kdt"}
    ]
  end
end
