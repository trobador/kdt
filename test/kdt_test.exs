defmodule KDTTest do
  use ExUnit.Case
  doctest KDT

  test "single node" do
    node = KDT.new(%KDT{point: {1, 2, 3}})
    assert node.point === {1, 2, 3}
  end
end
